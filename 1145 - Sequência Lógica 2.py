a, b = input().split()
a = int(a)
b = int(b)
c1 = 0
c2 = 0
while c1 < b:
    while c2 != a:
        print('{}'.format(c1+1), end=(""))

        c1 = c1 + 1
        c2 = c2 + 1
        if c2 > 0 and c2 < a:
            print(' ', end =(''))
    print('')
    c2 = 0  